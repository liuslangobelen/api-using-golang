package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"golang.org/x/crypto/bcrypt"
)

var db *gorm.DB
var err error

//User is a representation of a user
type User struct {
	Id int `json:"id"`
	jwt.StandardClaims
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required,gte=3"`
	Enable   string `json:"enable"`
}

//result is for the better response structure
type Result struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

//for JWT Auth
type M map[string]interface{}

var APPLICATION_NAME = "JWT for Test Yulius"
var LOGIN_EXPIRATION_DURATION = time.Duration(1) * time.Hour
var JWT_SIGNING_METHOD = jwt.SigningMethodHS256
var JWT_SIGNATURE_KEY = []byte("yulius")

func main() {
	db, err = gorm.Open("mysql", "root:@/gotest?charset:utf8&parseTime=True")

	if err != nil {
		log.Println("Connection failed", err)
	} else {
		log.Println("Connection established")
	}

	db.AutoMigrate(&User{})
	handleRequests()

}

func handleRequests() {
	log.Println("Started at http://127.0.0.1:8888")
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.Use(MiddlewareJWTAuthorization)
	myRouter.HandleFunc("/", index).Methods("GET")
	myRouter.HandleFunc("/api/create", createUser).Methods("POST")
	myRouter.HandleFunc("/api/login", login).Methods("POST")
	myRouter.HandleFunc("/api/afterLogin", afterLogin)
	myRouter.HandleFunc("/api/get", getUsers).Methods("GET")
	/*myRouter.HandleFunc("/api/get/{id}", getUser).Methods("GET")*/
	myRouter.HandleFunc("/api/update/{id}", updateUser).Methods("PUT")
	myRouter.HandleFunc("/api/delete/{id}", deleteUser).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8888", myRouter))
}

//function to create new user and save to database
func createUser(w http.ResponseWriter, r *http.Request) {
	payloads, _ := ioutil.ReadAll(r.Body)

	var user User
	json.Unmarshal(payloads, &user)
	err := db.Where("username = ?", user.Username).First(&user).Error
	cek := errors.Is(err, gorm.ErrRecordNotFound)
	if cek {
		temp, err := hashPassword(user.Password)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		user.Password = temp

		db.Create(&user)

		res := Result{Code: 200, Data: user, Message: "Success create user"}
		result, err := json.Marshal(res)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(result)
	} else {
		// error handling...
		http.Error(w, "username already exist", http.StatusBadRequest)
		return
	}

}

//function to login and get the token for JWT auth
func login(w http.ResponseWriter, r *http.Request) {
	var user User
	username, password, ok := r.BasicAuth()
	if !ok {
		http.Error(w, "Invalid username or password", http.StatusBadRequest)
		return
	}

	ok, userInfo := authenticateUser(username, password)
	if !ok {
		http.Error(w, "Invalid username or password", http.StatusBadRequest)
		return
	}

	claims := User{
		StandardClaims: jwt.StandardClaims{
			Issuer:    APPLICATION_NAME,
			ExpiresAt: time.Now().Add(LOGIN_EXPIRATION_DURATION).Unix(),
		},
		Username: userInfo.Username,
		Id:       userInfo.Id,
		Enable:   userInfo.Enable,
	}

	token := jwt.NewWithClaims(
		JWT_SIGNING_METHOD,
		claims,
	)

	signedToken, err := token.SignedString(JWT_SIGNATURE_KEY)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	tokenString, _ := json.Marshal(M{"token": signedToken})
	claims.Enable = signedToken
	db.Find(&user, "username = ?", username)
	db.Model(&user).Update(claims)
	w.Write([]byte(tokenString))

}

//to check every request that come into endpoint has a valid access token or not
func MiddlewareJWTAuthorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.URL.Path == "/api/login" {
			next.ServeHTTP(w, r)
			return
		}
		if r.URL.Path == "/api/create" {
			next.ServeHTTP(w, r)
			return
		}

		authorizationHeader := r.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			http.Error(w, "Invalid token", http.StatusBadRequest)
			return
		}

		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("signing method invalid")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("signing method invalid")
			}

			return JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(context.Background(), "userInfo", claims)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)

		// ...
	})
}

//check username and password for any user
func authenticateUser(username, password string) (bool, User) {

	var M User
	pass, err := hashPassword(password)
	if err != nil {
		return false, M
	}
	if err := db.Where("username = ? AND password >= ?", username, pass).Find(&M).Error; err != nil {
		return false, M
	}

	return true, M

}

//check user info after login
func afterLogin(w http.ResponseWriter, r *http.Request) {
	userInfo := r.Context().Value("userInfo").(jwt.MapClaims)
	message := fmt.Sprintf("hello %s (%s)", userInfo["username"], userInfo["enable"])
	w.Write([]byte(message))
}

//hashing user password
func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

//matching user password
func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

//only for checking
func index(w http.ResponseWriter, r *http.Request) {
	pass := "admin"
	hash, err := hashPassword(pass)
	cek := checkPasswordHash(pass, hash)
	fmt.Fprintf(w, "welcome!")
	fmt.Fprint(w, "pass : ", pass)
	fmt.Fprint(w, "hash : ", hash)
	fmt.Fprint(w, "check : ", cek)
	fmt.Fprint(w, "err : ", err)
}

//get all users data
func getUsers(w http.ResponseWriter, r *http.Request) {
	users := []User{}

	db.Find(&users)

	res := Result{Code: 200, Data: users, Message: "Success get users"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(results)
}

/*
func getUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]
	payloads, _ := ioutil.ReadAll(r.Body)

	var user User
	json.Unmarshal(payloads, &user)

	db.Find(&user, userID)

	res := Result{Code: 200, Data: user, Message: "Success get user"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(results)
}
*/

//updating selected user data using id
func updateUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]

	payloads, _ := ioutil.ReadAll(r.Body)

	var userUpdates User
	json.Unmarshal(payloads, &userUpdates)

	var user User
	db.Find(&user, userID)
	db.Model(&user).Update(userUpdates)

	res := Result{Code: 200, Data: user, Message: "Success update user"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

//deleting user data using id
func deleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]

	var user User

	db.Find(&user, userID)
	db.Delete(&user)

	res := Result{Code: 200, Data: user, Message: "Success delete user"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}
